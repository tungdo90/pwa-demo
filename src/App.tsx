import React, { useState } from "react";

import { fetchAPI } from "./api/fetch";
import "./App.css";
import PushNotificationDemo from "./PushNotificationDemo";

const App = () => {
  const [query, setQuery] = useState("");
  const [weather, setWeather] = useState({
    main: {
      temp: 0,
    },
    name: "",
    sys: {
      country: "",
    },
    weather: [],
  });

  const search = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const data = await fetchAPI(query);
      setWeather(data);
      setQuery("");
    } catch (e) {
      alert(e);
    }
  };

  const handleShareButton = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    const title = document.title;
    const url = document.location.href;
    const text = `The weather of ${weather.name} - ${weather.sys.country} is ${Math.round(weather.main.temp)} Celsius / ${weather.weather[0]["description"]}`

    // Check if navigator.share is supported by the browser
    if (navigator.share) {
      console.log("Congrats! Your browser supports Web Share API");
      navigator
          .share({
            url, title, text
          })
          .then(() => {
            console.log("Sharing successful");
          })
          .catch(() => {
            console.log("Sharing failed");
          });
    } else {
      alert("Sorry! Your browser does not support Web Share API");
    }
  };

  return (
    <div className="main-container">
      <form onSubmit={search}>
        <input
          type="text"
          className="search"
          placeholder="Search..."
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
      </form>
      {weather.main && weather.weather[0] && (
        <div className="city">
          <h2 className="city-name">
            <span>{weather.name}</span>
            <sup>{weather.sys.country}</sup>
          </h2>
          <div className="city-temp">
            {Math.round(weather.main.temp)}
            <sup>&deg;C</sup>
          </div>
          <div className="info">
            <img
              className="city-icon"
              src={`https://openweathermap.org/img/wn/${weather.weather[0]["icon"]}@2x.png`}
              alt={weather.weather[0]["description"]}
            />
            <p>{weather.weather[0]["description"]}</p>
          </div>
          <div className="share-wrapper">
            <button
              onClick={handleShareButton}
              className="share-button"
              type="button"
              title="Share this article"
            >
              <svg>
                <use href="#share-icon" />
              </svg>
              <span>Share</span>
            </button>
            <svg>
              <defs>
                <symbol
                  id="share-icon"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-share"
                >
                  <path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8" />
                  <polyline points="16 6 12 2 8 6" />
                  <line x1="12" y1="2" x2="12" y2="15" />
                </symbol>
              </defs>
            </svg>
          </div>
        </div>
      )}
      <PushNotificationDemo />
    </div>
  );
};

export default App;

